export class Task {
    description: string;
    due: Date;
}