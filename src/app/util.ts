export class Util {
    static convertISODateTioYYMMDDDD(isoDate: string): Date {
        const date = new Date(isoDate);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }
}