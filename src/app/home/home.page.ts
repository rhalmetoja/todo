import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  newItem: string;

  constructor(private todoService: TodoService) {}

  ngOnInit() {
    
  }

  add() {
    this.router.navigateByUrl('task');
  }

}
